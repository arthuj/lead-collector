package br.com.lead.collector.controllers;


import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired
    ProdutoService produtoService;

    @PostMapping()
    public Produto salvarProduto(@RequestBody Produto produto) {
        return produtoService.salvarProduto(produto);
    }

    @GetMapping()
    public Iterable<Produto> exibirTodos(@RequestParam(name = "nome", required = false) String nome)
    {
        if (nome != null) {
            Iterable<Produto> produtos = produtoService.buscarPorNome(nome);
            return produtos;
        }

        return produtoService.buscarTodos();
    }

    @GetMapping("/{id}")
    public Produto buscarPorID(@PathVariable int id)
    {
        try {
            Produto produto = produtoService.buscarPorId(id);
            return  produto;
        } catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Produto atualizarProduto(@PathVariable int id, @RequestBody Produto produto)
    {
        try {
            Produto produtoObjeto = produtoService.atualizarId(id,produto);
            return produtoObjeto;
        }catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deletarProduto(@PathVariable int id)
    {
        try{
            produtoService.deletarProduto(id);
            return ResponseEntity.status(201).body("");
        }catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }
}
