package br.com.lead.collector.enums;

public enum TipoLeadEnum {
    Quente,
    Frio,
    Organico;
}
