package br.com.lead.collector.services;


import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    ProdutoRepository produtoRepository;

    public Produto salvarProduto(Produto produto)
    {
        Produto produtoObjeto = produtoRepository.save(produto);
        return produtoObjeto;
    }

    public Iterable<Produto> buscarTodos()
    {
        Iterable<Produto> produtos = produtoRepository.findAll();
        return  produtos;
    }
    

    public Produto buscarPorId(int id)
    {
        Optional<Produto> optionalProdutos = produtoRepository.findById(id);
        if(optionalProdutos.isPresent())
            return optionalProdutos.get();
        throw new RuntimeException("O Produto não foi encontrado");

    }

    public Produto atualizarId(int id, Produto Produto)
    {
        if(produtoRepository.existsById(id))
        {
            Produto.setId(id);
            Produto produtoObjeto = salvarProduto(Produto);
            return produtoObjeto;
        }
        throw new RuntimeException("O Produto não foi encontrado");

    }

    public void deletarProduto(int id)
    {
        if(produtoRepository.existsById(id))
        {
            produtoRepository.deleteById(id);
        }
        else
            throw new RuntimeException("O Produto não foi encontrado");
    }

    public Iterable<Produto> buscarPorNome(String nome)
    {
        Iterable<Produto> produtos = produtoRepository.findAllByNomeContaining(nome);
        return produtos;
    }

}
